
@[toc]

# 安卓 Mediasoup V3 基于webrtc分支m84 的编译

>本文的前提已经 [正常编译webrtc](https://blog.csdn.net/CSqingchen/article/details/120016697)  
>本文首发地址 <https://blog.csdn.net/CSqingchen/article/details/120163087>  
>最新更新地址 <https://gitee.com/chenjim/chenjimblog>  


------


#### 相关网址链接  

mediasoup 官方地址 <https://mediasoup.org/github>  
mediasoup Github  <https://github.com/versatica>   
mediasoup 文档  <https://mediasoup.org/documentation/v3/>  


webrtc android static lib  
<https://github.com/haiyangwu/webrtc-android-build>       


`org.mediasoup.droid:mediasoup-client` 的源码：  
<https://github.com/haiyangwu/mediasoup-client-android>    

mediasoup 安卓示例，依赖 `mediasoup-client-android`  
<https://github.com/haiyangwu/mediasoup-demo-android>    

WebRTC-Mediasoup 编译记录，编译出 `libwebrtc.a` 及 `libmediasoupclient.a`  
https://leo-wxy.github.io/2020/09/24/WebRTC-Mediasoup%E7%BC%96%E8%AF%91%E8%AE%B0%E5%BD%95/


-----


#### mediasoup-client-android m79 编译  

>从 <https://github.com/haiyangwu/mediasoup-client-android> 可以看到，  
原库当前(2021年9月7日)基于webrtc m79编译，已经一年多未更新，[jcenter](https://jfrog.com/blog/into-the-sunset-bintray-jcenter-gocenter-and-chartcenter/) 也暂停更新服务。  
基于此fork修改了一份，参见 <https://github.com/chenjim/mediasoup-client-android>  
主要修改下载依赖的脚本 [scripts/get-dep.sh](https://github.com/chenjim/mediasoup-client-android/blob/dev/mediasoup-client/scripts/get-dep.sh)  

- 执行 [mediasoup-client/get_all_deps.sh](https://github.com/chenjim/mediasoup-client-android/blob/dev/mediasoup-client/get_all_deps.sh) 下载所需依赖  

-  [webrtc-android-build](https://github.com/haiyangwu/webrtc-android-build.git) 有大于100M的静态库文件,使用了`git lfs`   
需要 [安装相应的客户端](https://git-lfs.github.com/),执行`git lfs pull` 才能同步下来  
如果由于流量或者其他原因无法下载，可以 [自己编译](https://blog.csdn.net/CSqingchen/article/details/120016697)、求助 me@h89.cn    
- windows 下可以在 `Git Bash` 中执行脚本 `scripts/get-dep.sh`  

- 用 [Android Studio](https://developer.android.com/studio) 打开工程编译吧。。。


-----


#### mediasoup-client-android 更新到 m84 

1. 更新webrtc相关库到 4147 m84  
切换到 `branch-heads/4147` 分支： `git co -b m84 branch-heads/4147`  
参考 [webrtc编译](https://blog.csdn.net/CSqingchen/article/details/120016697) 编译静态库 libwebrtc.a 和 libwebrtc.jar  
替换 `mediasoup-client-android\mediasoup-client\deps\webrtc\lib\` 中相应内容

2. 更新 [.\mediasoup-client\deps\libmediasoupclient](https://github.com/versatica/libmediasoupclient) 到 3.2.0  
  切换到 [tag 3.2.0](https://github.com/versatica/libmediasoupclient/tree/3.2.0) 节点：`git checkout -b 320 3.2.0`，然后修改如下:   
  ``` diff 
  --- a/CMakeLists.txt
  +++ b/CMakeLists.txt
  @@ -128,10 +128,21 @@ target_include_directories(${PROJECT_NAME} PUBLIC
  )

  # Public (interface) dependencies.
  +if("${ANDROID_ABI}" STREQUAL "")
  target_link_libraries(${PROJECT_NAME} PUBLIC
          sdptransform
          ${LIBWEBRTC_BINARY_PATH}/libwebrtc${CMAKE_STATIC_LIBRARY_SUFFIX}
  )
  +else()
  +       # Add '-whole-archive' to keep symbols from peerconnection_jni.
  +       # https://stackoverflow.com/a/5687860/2085408
  +       SET (webrtc -Wl,--whole-archive ${LIBWEBRTC_BINARY_PATH}/${ANDROID_ABI}/libwebrtc${CMAKE_STATIC_LIBRARY_SUFFIX}  -Wl,--no-whole-archive)
  +
  +       target_link_libraries(${PROJECT_NAME} PUBLIC
  +                       sdptransform
  +                       ${webrtc}
  +                       )
  +endif()

  # Compile definitions for libwebrtc.
  target_compile_definitions(${PROJECT_NAME} PUBLIC
  ```

3. 用 [Android Studio](https://developer.android.com/studio) 打开工程编译吧。。。  
在 `mediasoup-client-android\mediasoup-client\build\outputs\aar`会有编译的aar，可供 [mediasoup-demo-android](https://github.com/haiyangwu/mediasoup-demo-android) 使用  
如：替换 `implementation 'org.mediasoup.droid:mediasoup-client:3.0.8-beta-3'`   
为 `implementation files('libs/media_client-debug.aar')`  


**备注:**  
修改后的结果，可以同步到对应的仓库，  
然后修改 `mediasoup-client\scripts\get-dep.sh` 中仓库配置  
下次执行 `mediasoup-client\get_all_deps.sh` 下载所有依赖。。。。。

-----


#### libmediasoupclient 编译使用

从 [mediasoup installation](<https://mediasoup.org/documentation/v3/libmediasoupclient/installation>) 中看到  
当前(2021年8月24日) 支持 [webrtc.googlesource](https://webrtc.googlesource.com/src.git) branch-heads/4147 (m84)   

**注意注意注意：** 这里编译结果只适用编译的平台，如ubuntu，不适用安卓！！！  


```bash 
$ git clone https://github.com/versatica/libmediasoupclient
$ cd libmediasoupclient/
$ git checkout 3.X.Y.

$ export PATH_MCA_WRTC=/home/chen/code/mediasoup/mediasoup-client-android/mediasoup-client/deps/webrtc

$ export PATH_TO_LIBWEBRTC_SOURCES=$PATH_MCA_WRTC/src

$ export PATH_TO_LIBWEBRTC_BINARY=$PATH_MCA_WRTC/lib

$ cmake . -Bbuild \
  -DLIBWEBRTC_INCLUDE_PATH:PATH=${PATH_TO_LIBWEBRTC_SOURCES} \
  -DLIBWEBRTC_BINARY_PATH:PATH=${PATH_TO_LIBWEBRTC_BINARY}

$ make -C build/
```



参考自    
<https://mediasoup.org/documentation/v3/libmediasoupclient/installation>



-----

其它相关文档  
- 安卓webrtc在ubuntu 2004下编译使用  
<https://blog.csdn.net/CSqingchen/article/details/120016697>

- Android 断点调试 webrtc、 medieasoup   
<https://blog.csdn.net/CSqingchen/article/details/120156900>  

- 安卓增加 mediasoup webrtc 日志输出  
<https://blog.csdn.net/CSqingchen/article/details/120156669>  

- 安卓 Mediasoup V3 基于webrtc 分支m84 的编译  
<https://blog.csdn.net/CSqingchen/article/details/120163087>

- 安卓 webrtc 开启h264 软编解码  
<https://blog.csdn.net/CSqingchen/article/details/120199702>  

- 安卓mediasoup输出H264流(支持H264编码)  
<https://blog.csdn.net/CSqingchen/article/details/120218832>  

- 安卓mediasoup webrtc h264 软编解码相关源码分析  
<https://blog.csdn.net/CSqingchen/article/details/120218923>  


----

