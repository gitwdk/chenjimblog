
# 安卓增加 mediasoup webrtc 日志输出

@[toc]

>本文首发地址 <https://blog.csdn.net/CSqingchen/article/details/120156669>  
>最新更新地址 <https://gitee.com/chenjim/chenjimblog>  

### 增加 webrtc 日志  
  修改 `mediasoup-client/src/main/jni/jni_onload.cpp` 如下

  ```diff
  @@ -12,6 +12,7 @@ extern "C" jint JNIEXPORT JNICALL JNI_OnLoad(JavaVM* jvm, void* reserved)
    if (ret < 0)
      return -1;

  +	rtc::LogMessage::LogToDebug(rtc::LoggingSeverity::LS_INFO);
    mediasoupclient::Initialize();
    webrtc::jni::LoadGlobalClassReferenceHolder();
    return JNI_VERSION_1_6;

  ```


  参考自 [https://github.com/lihuan545890/mediasoup-client-android/](https://github.com/lihuan545890/mediasoup-client-android/commit/f6acd346083dc28a68a87e27225f9d69c25e3fe1#diff-59aae254b6ceabf6275ecfc7f1c72274886dfd6c02f8f89ebbffca29fa66da85)  


-----

### 增加 libmediasoupclient 日志  
  修改 `\mediasoup-client-android\mediasoup-client\deps\libmediasoupclient\src\Logger.cpp`  
  ```diff
  -   Logger::LogLevel Logger::logLevel = Logger::LogLevel::LOG_NONE;
  +   Logger::LogLevel Logger::logLevel = Logger::LogLevel::LOG_TRACE;
  ```
  修改 `mediasoup-client-android\mediasoup-client\deps\libmediasoupclient\include\Logger.hpp`  
  ```diff
  -  if (Logger::handler && Logger::logLevel == Logger::LogLevel::LOG_DEBUG) \
  +  if (Logger::handler && Logger::logLevel >= Logger::LogLevel::LOG_DEBUG) \
  ```



-----



### 增加 mediasoup-client 日志  
  修改   
  `mediasoup-client-android\mediasoup-client\src\main\java\org\mediasoup\droid\Logger.java`    
  ```diff 
  -    Logger.setLogLevel(Logger.LogLevel.LOG_DEBUG);
  +    Logger.setLogLevel(Logger.LogLevel.LOG_TRACE);
  ```


  ----
